---
layout: post
title: "macOS High Sierra Bugs"
---

系统环境：

MacBook Pro with Retina Display 13'' 2014 Mid

所有设置均为默认，FileVault保持开启。



1. 换壁纸之后，启动登陆界面壁纸并未更改
2. Safari在遇到某些网站（尤其是使用过时技术的）时，关闭标签页后显示错乱
3. 更改过客人用户设置后，在启动界面会出现第二个客人用户图表，而且头像颜色错乱.
4. 更改过需要密码解锁的设置后，节能设置错乱
5. Finder有些时候会花屏


Build With: [Emacs](https://gnu.org/software/emacs), [XMXE](https://gitee.com/ReimuXMX/XMXE)

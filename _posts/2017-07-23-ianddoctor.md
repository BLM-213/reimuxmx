---
layout: post
title: "我和医生"
---

![Awesome WM](http://i2.kiimg.com/1949/1ae15bcb0dd9f666.png)
  
  
(配图是Awesome WM的配置文件……）

我去看病，从来没遭遇过医生的反感和不耐心。

因为我深知不靠谱的需求和重复的工作是多么让人生气……

但是能认出我工作的，今天还是头一个：

我今天有点感冒，趁着休息的时候去了医院，

因为我赶时间，所以就随便挂了个呼吸科的普通号。

进去一看，没有别的病人。

医生：哪儿不舒服？

我：感觉有点感冒，不怎么咳嗽，但是流黄鼻涕balabala……总之把我能知道的都叙述了一遍。

医生：呃，先抽个血化验一下吧。

我：拿单子走人，某全球最大医院的速度不是盖的，三十分钟后结果出来了。看着没啥大事情。

医生：（看完结果后）确实有点上呼吸道感染了，白细胞有点高。你是吃药还是打针？

我：吃药吧，我还得上班呢……另外我没啥过敏的药。

医生：行……我给你开点左克（左氧氟沙星的商品名），一天三次一次两粒，有啥不舒服的再来复查。

我：行，谢谢大夫。

（开药方和打印ing……）

医生：你是程序员吧？

我：（惊讶）呃，是，您怎么知道的？

医生：我看过的病人里，能这么明确和完整地描述自己的症状和身体情况，而且还不问个不停的，除了我们医生自己，就是程序员了…………

我：…………
